#!/usr/bin/env bash
mkdir -p $HOME/.atom
for F in config.cson init.coffee keymap.cson snippets.cson styles.less; do
    ln -s $PWD/$F $HOME/.atom/$F
done
apm stars --user abrenecki --install
