child_process = require 'child_process'

atom.commands.add 'atom-text-editor', 'tools:run-isort', ->
  return unless editor = atom.workspace.getActiveTextEditor()
  if editor.isModified()
    editor.save()
  path = editor.getPath()
  child_process.spawn "isort", [path]

atom.commands.add 'atom-text-editor', 'tools:insert-import', ->
  return unless editor = atom.workspace.getActiveTextEditor()
  if editor.isModified()
    editor.save()
  path = editor.getPath()
  importline = prompt("Enter your import:")
  child_process.spawn "isort", ['-a', importLine, path]

atom.commands.add 'atom-text-editor', 'tools:run-yapf-on-selection', ->
  return unless editor = atom.workspace.getActiveTextEditor()
  if editor.isModified()
    editor.save()
  path = editor.getPath()

  # TODO: support multiple ranges
  range = editor.getSelectedBufferRange()
  args = [path, "--in-place", "--lines", "#{range.start.row}-#{range.end.row}"]
  console.log "yapf", args
  child_process.spawn "yapf", args


# TODO: atom command to run ctags on working directory (without arguments,
# assume a .ctags file is present)
# TODO: run ctags on save
